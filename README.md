# Dotfiles
**Date**: 31 of March of 2022

My current dotfiles. 

## Configs already added
- i3
- Rofi
- Polybar
- Conky

## APPs
- **WM**: i3
- **Launcher**: Rofi
- **Bar**: Polybar
- **Calculator**: Qalculate!
- **Clipboard Manager**: Clipmenu
- ~~**Email Client**~~
- ~~**Notes**~~
- **System information**: Conky
- **Terminal**: Alacritty
- **Music**: spotify-qt && spotifyd

## To config
- [X] ArcoLinux logout
- [ ] Notifications
- [X] ~~i3bar~~
- [X] Clipboard
- [X] Conky
- [X] Calculator
- [X] ~~i3msg~~
- [X] SDDM
- [X] Variety
- [ ] Clean dots
- [ ] Neovim
- [ ] DT Polybar
