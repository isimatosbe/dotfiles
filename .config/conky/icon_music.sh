# Script to print player icon

player=$(playerctl -l | grep -v "spotifyd" | head -1 | sed -r "s/(.+)?\.+.*/\1/")

metadata=$(playerctl -p "$player" metadata)

case "$player" in
	"spotify"|"spotify-qt")
	echo ""
	;;
	"firefox")
	echo ""
	;;
	"")
	echo ""
	;;
	*)
	echo ""
esac