# Script to get artist and title

player=$(playerctl -l | grep -v "spotifyd" | head -1 | sed -r "s/(\S+)?\..+/\1/")
#title=$(playerctl -p "$player" metadata | grep title | sed -r "s/\S+\s+\S+\s+(.+)/\1/")
title=$(playerctl -p "$player" metadata --format "{{ title }}")
#artist=$(playerctl -p "$player" metadata | grep artist | sed -r "s/\S+\s+\S+\s+(.+)/\1/")
artist=$(playerctl -p "$player" metadata --format "{{ artist }}")

# get_info prints the title/artist or No player found/Nothing
get_info()
{
if [ "$1" == "title" ];
then
	if [ "$player" != "" ];
	then
		if [ expr length "$title" > 35 ];
		then
			echo "$(echo $title | fold -35 | head -1) ..."
		else
			echo "$title"
		fi
	fi

elif [ "$1" == "artist" ];
then
	if [ "$player" != "" ];
	then
		if [ expr length "$artist" > 30 ];
		then
			echo "by $(echo $artist | fold -30 | head -1) ..."
		elif [ "$artist" != "" ];
		then
			echo "by $artist"
		elif [ "$title" == "" ];
		then 
			echo "Nothing is playing"
		fi
	else
		echo "No player found"
	fi
fi
}

echo "$(get_info $1)"