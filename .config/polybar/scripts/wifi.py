# Program to get wifi status and connection.
# For the wifi icons I used "DejaVu Sans Mono wifi ramp:size=13.5;2.5"
# from here https://github.com/isaif/polybar-wifi-ramp-icons

import re
import subprocess

wifi = ["","","","","",""] # Wifi icons
wifi_connection = "wlp92s0" # Check yours with ip link

def main() :
	reg_con = wifi_connection + "\s+wifi\s+(.*?)\s"
	reg_name = wifi_connection + "\s+wifi\s+connected\s+(.*?)\s"
	reg_qua = wifi_connection + ":\s[0-9]*\s+([0-9]+)."

	res_1 = subprocess.getstatusoutput("nmcli device")[1]

	con = re.findall(reg_con,res_1)[0]
	if con == 'connected':
		name = re.findall(reg_name,res_1)[0]

		res_2 = subprocess.getstatusoutput("cat /proc/net/wireless")[1]
		qua = int(re.findall(reg_qua,res_2)[0])
		i = qua//14 + 1
	elif con == 'disconnected':
		name = "No Connection"
		i = 0
	else:
		name = "Error"
		i = 0

	return wifi[i] + " " + name

if __name__ == "__main__":
	print(main())
